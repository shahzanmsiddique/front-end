# MamunTask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.4.

## Other libraries used
[Tailwindcss](https://tailwindcss.com/)  
[Angular material](https://material.angular.io/)

## Install dependencies
After cloning into folder , run `npm install` to install dependencies.

## Development server
Add `socketURL` property in prod.environment and environment files in src/environments/ folder in case websocket url used is different.  
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Screenshots
Screenshots of app are available in SCREEN_SHOTS folder.
