import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CreditCardService {
  private ccNo = new Subject<String>();
  private cvv = new Subject<String>();

  constructor() {}
  changeCcNo(value: string) {
    this.ccNo.next(value);
  }
  getCcNo(): Observable<any> {
    return this.ccNo.asObservable();
  }
  changeCvv(value: string) {
    this.cvv.next(value);
  }
  getCvv(): Observable<any> {
    return this.cvv.asObservable();
  }
}
