import { CreditCardService } from './../services/credit-card.service';
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[credit-card]',
})
// used to mask credit card number and security code on UI
export class CreditCardDirective {
  constructor(
    private creditCardService: CreditCardService,
    private el: ElementRef
  ) {}

  @Input('ccType') ccType: string = 'ccNo';
  @HostListener('input', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;
    let trimmed = input.value.replace(/\D/g, '');
    switch (this.ccType) {
      case 'ccNo':
        {
          trimmed = trimmed.replace(/\ss+/g, '');
          if (trimmed.length > 16) {
            trimmed = trimmed.substring(0, 16);
          }

          let numbers = [];
          for (let i = 0; i < trimmed.length; i += 4) {
            numbers.push(trimmed.substring(i, i + 4));
          }

          input.value = numbers.join('  ');
          this.creditCardService.changeCcNo(numbers.join('  '));
        }
        break;
      case 'cvv':
        {
          if (trimmed.length > 3) {
            trimmed = trimmed.substring(0, 3);
          }
          input.value = trimmed;
          this.creditCardService.changeCvv(trimmed);
        }
        break;
    }
  }
}
