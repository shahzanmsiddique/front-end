import { environment } from './../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { delay, retryWhen, Subscription } from 'rxjs';
import { CreditCardService } from '../services/credit-card.service';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.scss'],
})
export class CreditCardComponent implements OnInit {
  form: FormGroup = new FormGroup({
    amount: new FormControl('', [
      Validators.required,
      Validators.min(0.01),
      Validators.max(1000),
    ]),
    currency: new FormControl('OMR', Validators.required),
    name: new FormControl('', [
      Validators.required,
      Validators.maxLength(100),
      Validators.pattern('[a-zA-Z  -]*'),
    ]),
    cardNo: new FormControl('', [
      Validators.required,
      Validators.minLength(22),
    ]),
    expiryMonth: new FormControl('', Validators.required),
    expiryYear: new FormControl('', Validators.required),
    cvv: new FormControl('', [Validators.required, Validators.minLength(3)]),
    isToSave: new FormControl(false),
  });

  ccNoSub: Subscription = new Subscription();
  cvvSub: Subscription = new Subscription();
  years: Array<number> = [];
  months: Array<string> = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
  ];
  minMonth: String = '';
  maxMonth: String = '';
  mm: any = '';
  yy: number = 0;
  isConnected = false;
  showHint = false;
  myWebSocket: WebSocketSubject<any> = webSocket({
    url: environment.socketURL,
    openObserver: {
      next: () => {
        console.log('connetion ok');
        this.isConnected = true;
      },
    },
    closeObserver: {
      next: () => {
        console.log('connetion fail');
        this.isConnected = false;
      },
    },
  });
  isLoading = false;
  showErr = false;
  showSuccess = false;
  amount = '';
  constructor(private creditCardService: CreditCardService) {}

  ngOnInit(): void {
    const today = new Date();
    const mm = today.getMonth() + 1;
    this.mm = mm < 10 ? '0' + mm : mm;
    this.yy = Number(today.getFullYear());
    this.years = this.generateArrayOfYears();
    this.ccNoSub = this.creditCardService.getCcNo().subscribe((value) => {
      this.form.get('cardNo')?.setValue(value);
    });
    this.ccNoSub = this.creditCardService.getCvv().subscribe((value) => {
      this.form.get('cvv')?.setValue(value);
    });

    this.myWebSocket
      .pipe(retryWhen((errors) => errors.pipe(delay(50)))) //try to re-connect to socket in  case of failure
      .subscribe(
        (msg) => {
          console.log(msg);
          this.hide();
          this.amount = msg.amount;
          if (msg.success) {
            this.showSuccess = true;
          } else {
            this.showErr = true;
          }
        },
        (err) => {
          console.log(err);
        },
        () => console.log('complete')
      );
  }

  generateArrayOfYears() {
    var min = new Date().getFullYear();
    var max = min + 9;
    var years = [];

    for (var i = min; i <= max; i++) {
      years.push(i);
    }
    return years;
  }

  yearChange() {
    if (
      this.form.value.expiryYear === this.yy &&
      this.form.value.expiryMonth <= this.mm
    ) {
      this.form.get('expiryMonth')?.setValue('');
    }
  }
  hide() {
    this.amount = '';
    this.showErr = this.showSuccess = false;
  }

  onSubmit() {
    this.hide();
    if (this.isConnected) {
      this.myWebSocket.next({
        ...this.form.value,
        expiry: `${this.form.value.expiryMonth}/${this.form.value.expiryYear
          .toString()
          .substring(2)}`,
      });
    } else {
      this.showErr = true;
    }
  }

  ngOnDestroy() {
    this.myWebSocket.unsubscribe();
    this.ccNoSub.unsubscribe();
    this.cvvSub.unsubscribe();
  }
}
