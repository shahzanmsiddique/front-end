const colors = require("tailwindcss/colors");

module.exports = {
  mode: "jit",
  prefix: "tw-", //to prevent conflict with other libraries
  important: true,
  purge: {
    enabled: true,
    content: ["./src/**/*.{html,ts}"],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      purple: colors.violet,
      indigo: colors.indigo,
      gray: colors.coolGray,
      red: colors.red,
    },
    fontFamily: {
      lato: "'Lato', serif",
    },
    extend: {
      zIndex: {
        "-10": "-10",
      },
    },
  },
  variants: {
    backgroundColor: [
      "responsive",
      "dark",
      "group-hover",
      "focus-within",
      "hover",
      "focus",
    ],
    extend: {},
  },
  plugins: [],
};
